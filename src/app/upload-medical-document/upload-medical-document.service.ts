import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { CONFIG } from '../constant/config';

@Injectable()
export class UploadMedicalDocumentService {
  constructor(private http: Http) { }
  uploadingDocumentToServer(FD: Object) {
    return this.http.post(CONFIG.API_ENDPOINT + 'SelfSurveyServices/document', FD)
      .map((res: Response) => {
        if (res) {
          if (res.status === 201) {
            return res.json();
          } else if (res.status === 200) {
            return res;
          }
        }
      }).catch((error: any) => {
        if (error.status < 400 || error.status === 500) {
          return error;
        }
      });
  }
}