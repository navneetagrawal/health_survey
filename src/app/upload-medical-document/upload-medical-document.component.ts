import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute, Params } from '@angular/router';
import { SessionService } from '../constant/sessionService.component.service';
import { Ng2DeviceService } from 'ng2-device-detector';
import { UploadMedicalDocumentService } from './upload-medical-document.service'
declare var $: any;

@Component({
  selector: 'app-upload-medical-document',
  templateUrl: './upload-medical-document.component.html',
  providers: [UploadMedicalDocumentService]
})
export class UploadMedicalDocumentComponent implements OnInit {
  savePage: boolean = false;
  afterSaveClick: boolean = false;
  progressArray: Array<Object> = [];
  finalUploadArray: Array<any> = [];
  uploadedImageCollection: Array<any> = [];
  imageArrayForUpload: Array<any> = [];
  creationOfUploadArray: Array<any> = [];
  anotherUploadArray: Array<any> = [];
  recordName: string;
  geolat: string;
  geolong: string;
  activityDetail: Object;
  deviceInfo: any;
  sendTarget: any;
  constructor(private _uploadMedicalDocumentService: UploadMedicalDocumentService, private router: Router, private _sessionService: SessionService, private _deviceService: Ng2DeviceService) {
    let sendTarget: NavigationExtras = {
      queryParams: {
        target: this.router.url.split('=')[1]
      }
    };
    this.sendTarget = sendTarget;
    if (this._sessionService.uploadMedicalDocumentPage == false && this._sessionService.successPage == true) {
      this.router.navigate(['/success'], sendTarget);
      return;
    }
  }
  onDocumentChange(data: any, setButtonClicked: string) {
    if (data.files.length == 0) {
      return;
    }
    if (setButtonClicked == 'Next') {
      this.progressArray = [];
      this.recordName = '';
    }
    $("#resendEmailPolicySchedule").modal("hide");
    this.savePage = true;
    var file = data.files;
    for (var x = 0; x < file.length; x++) {
      this.finalUploadArray.push(file[x]);
      var reader = new FileReader();
      reader.onload = (event: any) => {
        var showObj = {}
        showObj['url'] = event.target.result;
        this.progressArray.push(showObj);
      }
      reader.readAsDataURL(file[x]);
    }
  }
  forAnotherUpload(fileName: any) {
    this.anotherUploadArray.forEach(element => {
      if (element.fileName == fileName['name']) {
        this.progressArray = element['progressArray'];
        this.savePage = true;
        this.recordName = fileName['name'];
      }
    });
  }
  
  uploadDocument() {
    var anotherUploadObj = {
      fileName: this.recordName,
      progressArray: this.progressArray
    };
    this.anotherUploadArray.push(anotherUploadObj);
    var file = this.finalUploadArray;
    var fileObj = {
      fileArray: file
    }
    this.creationOfUploadArray.push(fileObj);
    this.finalUploadArray = [];
    var date = (new Date().getTime());
    //var actID = this.activityDetail['activityId'];
    // var len = this.activityDetail['activityId'].toString().length;
    // if (len != 10) {
    //   for (var i = len + 1; i <= 10; i++) {
    //     actID = "0" + actID;
    //   }
    // }
    var objToSend = {
      //"activityId": this.activityDetail['activityId'],
      "geoLocation": {
        "isBlocked": false,
        "deviceName": this.deviceInfo['device'],
        "latitude": (this.geolat) ? this.geolat : "",
        "longitude": (this.geolong) ? this.geolong : "",
        "operatingSystem": navigator.appVersion,
        "timezone": Intl.DateTimeFormat().resolvedOptions().timeZone,
        "deviceTime": date,
        "ipAddress": "",
        "deviceIMEI": "",
        "macAddress": ""
      },
      "isLast": false,
      "isLastDocument": false,
    }
    var FD = new FormData();
    for (var x = 0; x < this.creationOfUploadArray[this.creationOfUploadArray.length - 1]['fileArray'].length; x++) {
      var fileNameValidation = this.creationOfUploadArray[this.creationOfUploadArray.length - 1]['fileArray'][x]['name'].substr(this.creationOfUploadArray[this.creationOfUploadArray.length - 1]['fileArray'][x]['name'].lastIndexOf('.') + 1);
      if (fileNameValidation.toUpperCase() == 'PNG' || fileNameValidation.toUpperCase() == 'JPG'
        || fileNameValidation.toUpperCase() == 'JPEG') {
        var imageType = this.recordName + '_' + x + '.jpg';
      } else {
        var imageType = this.recordName + '_' + x + '.pdf';
      }
      var fileName = "actID" + imageType;
      FD.append("file", this.creationOfUploadArray[this.creationOfUploadArray.length - 1]['fileArray'][x], fileName);
    }
    FD.append("documentData", JSON.stringify(objToSend));
    if (this.imageArrayForUpload.length == 0) {
      this.imageArrayForUpload.push(FD);
      this.uploadImageToServer();
    } else {
      this.imageArrayForUpload.push(FD);
    }
    this.functionToRedirect();
  }
  functionToRedirect() {
    var nameFound = false;
    if (this.uploadedImageCollection.length == 0) {
      var obj = {
        name: this.recordName,
        noOfImages: this.progressArray.length
      }
      this.uploadedImageCollection.push(obj);
    } else {
      for (var i = 0; i < this.uploadedImageCollection.length; i++) {
        if (this.uploadedImageCollection[i].name == this.recordName) {
          nameFound = true;
          var index = i;
        }
      };
      if (nameFound) {
        this.uploadedImageCollection[index].noOfImages = this.progressArray.length;
      } else {
        obj = {
          name: this.recordName,
          noOfImages: this.progressArray.length
        }
        this.uploadedImageCollection.push(obj);
      }
    }
    this.savePage = false;
    this.afterSaveClick = true;
  }
  uploadImageToServer() {
    if (this.imageArrayForUpload.length > 0) {
      this._uploadMedicalDocumentService.uploadingDocumentToServer(this.imageArrayForUpload[0]).subscribe(data => {
        this.imageArrayForUpload.splice(0, 1);
      }, error => {
        this.finalUploadArray = [];
        //this.uploadImageToServer();
        this.imageArrayForUpload.splice(0, 1);
        $('#uploadDocument').val('');
      });
    }
  }
  getLocation() {
    var self = this;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position: any) {
        self.geolat = position.coords.latitude.toString();
        self.geolong = position.coords.longitude.toString();
      });
    }
  }
  goToSuccess() {
    this._sessionService.successPage = true;
    this.router.navigate(['/success']);
  }
  ngOnInit() {
    if (performance.navigation.type == 1) {
      this._sessionService.uploadMedicalDocumentPage = true;
    }
    var token = this._sessionService.uploadMedicalDocumentPage;
    if (token == false) {
      history.back();
      return;
    }
    this._sessionService.instructionPage = false;
    this.activityDetail = JSON.parse(this._sessionService.getActivityDetails("activityObject"));
    // if (this.activityDetail == null) {
    //   this.router.navigate(['/splash'], this.sendTarget);
    //   return;
    // }
    this.deviceInfo = this._deviceService.getDeviceInfo();
    this.getLocation();
  }

}
