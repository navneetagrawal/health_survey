import { Component, OnInit } from '@angular/core';
import { SessionService } from '../constant/sessionService.component.service';
@Component({
  selector: 'success',
  templateUrl: './success.html'
})
export class SuccessComponent {
  constructor(private _sessionService: SessionService) { }
  ngOnInit(): void {
    if (performance.navigation.type == 1) {
      this._sessionService.successPage = true;
    }
    var token = this._sessionService.successPage;
    if (token == false) {
      history.back();
      return;
    }
    this._sessionService.uploadMedicalDocumentPage = false;
  }
}
