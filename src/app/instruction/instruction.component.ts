import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute, Params } from '@angular/router';
import { CONFIG } from '../constant/config';
import { SessionService } from '../constant/sessionService.component.service';
@Component({
	selector: 'instruction',
	templateUrl: './instruction.html'
})
export class InstructionComponent {
	subMainContent: any;
	instructionList: any;
	sendTarget: any;
	deviceInfo: Object;
	activityDetail: Object;
	instructionListDetails: any;
	title: string;
	constructor(private router: Router, private _sessionService: SessionService) {
		this._sessionService.isHeader = true;
		let sendTarget: NavigationExtras = {
			queryParams: {
				target: this.router.url.split('=')[1]
			}
		};
		this.sendTarget = sendTarget;
		// if (this._sessionService.instructionPage == false && this._sessionService.uploadMedicalDocumentPage == true) {
		// 	this.router.navigate(['/uploadDocument'], sendTarget);
		// 	return;
		// }
	}
	gotoUpload() {
		this._sessionService.uploadMedicalDocumentPage = true;
		this.router.navigate(['/uploadDocument'], this.sendTarget);
	}
	whenPageLoads() {
		var self = this;
		this.title = this.instructionListDetails.title;
		self.subMainContent = this.instructionListDetails.description;
		[this.activityDetail['additionalDetails']['Registration Number']].forEach(function (tag) {
			self.subMainContent = self.subMainContent.replace('%s', tag);
		});
		this.instructionList = this.instructionListDetails.TSurveyInstructions;
	}
	ngOnInit(): void {
		if (performance.navigation.type == 1) {
			this._sessionService.instructionPage = true;
		}
		// var token = this._sessionService.instructionPage;
		// if (token == false) {
		// 	history.back();
		// 	return;
		// }
		// this.activityDetail = JSON.parse(this._sessionService.getActivityDetails("activityObject"));
		// if (this.activityDetail == null) {
		// 	this.router.navigate(['/splash'], this.sendTarget);
		// 	return;
		// }
		this.instructionListDetails = JSON.parse(this._sessionService.getInstruction("instructionObject"));
		this.instructionListDetails.TSurveyInstructions.sort(function (a: any, b: any) { return a.surveyInstructionsId - b.surveyInstructionsId; })
		this.whenPageLoads();
	}
}
