export class CONFIG {
	//For Local and Dev Server
	public static API_ENDPOINT = 'https://service-develb.godigit.com/';
	public static API_ENDPOINT_COMMONDATASERVICE = 'https://service-develb.godigit.com/';

	//For Pre Production
	//public static API_ENDPOINT = 'https://preprod-SelfSurveyServices.godigit.com/';
	//public static API_ENDPOINT_COMMONDATASERVICE = 'https://preprod-CommonDataService.godigit.com/';

	//For Producction
	//public static API_ENDPOINT = 'https://prod-SelfSurveyServices.godigit.com/';
	//public static API_ENDPOINT_COMMONDATASERVICE = 'https://prod-CommonDataService.godigit.com/';
	public static ACTIVITY_TYPE = {
	};
}