import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SessionService {
    instructionPage: boolean;
    uploadMedicalDocumentPage: boolean;
    successPage: boolean;
    isHeader: boolean;
    constructor() {
        this.instructionPage = false;
        this.uploadMedicalDocumentPage = false;
        this.successPage = false;
    }
    public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    display(value: boolean) {
        this.status.next(value);
    }
    setActivityDetails(value: Object) {
        return sessionStorage.setItem("activityObject", JSON.stringify(value));
    }
    getActivityDetails(key: string) {
        return sessionStorage.getItem(key);
    }
    setInstruction(obj: Object) {
        return sessionStorage.setItem("instructionObject", JSON.stringify(obj));
    }
    getInstruction(key: string) {
        return sessionStorage.getItem(key);
    }
    clearSession() {
        return sessionStorage.clear();
    }
}