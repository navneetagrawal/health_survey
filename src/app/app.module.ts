import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { SplashComponent, InstructionComponent, SuccessComponent, UploadMedicalDocumentComponent } from './directives';
import { SessionService } from './constant/sessionService.component.service';



@NgModule({
  declarations: [
    AppComponent,
    InstructionComponent,
    SplashComponent,
    SuccessComponent,
    UploadMedicalDocumentComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes, { useHash: true }),
    NgbModule.forRoot(),
    Ng2DeviceDetectorModule.forRoot(),
    Ng2AutoCompleteModule
  ],
  providers: [SessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
