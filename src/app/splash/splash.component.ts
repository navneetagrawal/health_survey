import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute, NavigationExtras } from '@angular/router';
import { SplashService } from './splash.service';
import { SessionService } from '../constant/sessionService.component.service';
@Component({
	selector: 'splash',
	templateUrl: './splash.html',
	providers: [SplashService]
})
export class SplashComponent {
	isDesktop: boolean;
	isValid: boolean;
	isdata: boolean;
	constructor(private router: Router, private route: ActivatedRoute, private _splashService: SplashService, private _sessionService: SessionService) {
		this._sessionService.isHeader = false;
	}
	getActivityAndInstruction() {
		this.route.queryParams.forEach((params: Params) => {
			var target = params['target'];
			if (target == undefined) {
				return;
			}
			this._splashService.getActivityDetails(target).subscribe(data => {
				if (data.status === 204 || data.length == 0) {
					this.isValid = true;
					return;
				}
				var actTypeID = data[0].activityTypeId;
				var obj = {
					activityId: data[0].activityId,
					activityTypeId: data[0].activityTypeId,
					policyNumber: data[0].policyNumber,
					productId: data[0].productId,
					documentDetails: data[0].documentDetails,
					additionalDetails: data[0].additionalDetails
				}
				this._sessionService.setActivityDetails(obj);
				this._splashService.getInstructionDetails(actTypeID).subscribe(data => {
					if (data.status === 204) {
						this.isdata = true;
						return;
					}
					this._sessionService.setInstruction(data);
					let navigationExtras: NavigationExtras = {
						queryParams: {
							target: params['target']
						}
					};
					this._sessionService.instructionPage = true;
					this.router.navigate(['/instruction'], navigationExtras);
				}, error => {
				});
			}, error => {
			});
		});
	}
	goToPlayStore(){
		var encryptedData = location.href.split("=")[1]
		var playStoreLink = "intent://digit/?target=" + encryptedData + "#Intent;scheme=splash;package=com.digit_insurance;end";
		location.href = playStoreLink;
	}
	ngOnInit(): void {
		sessionStorage.clear();
		var isMobile = /iPhone|iPad|iPod|Android|Windows Phone/i.test(navigator.userAgent);
		var isAndroid = /Android/i.test(navigator.userAgent);
		if (isMobile) {
			if (isAndroid) {
				this.getActivityAndInstruction();
			} else {
				this.getActivityAndInstruction();
			}
		} else {
			this.isDesktop = true;
		}
	}
}

