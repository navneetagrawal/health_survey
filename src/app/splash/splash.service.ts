import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { CONFIG } from '../constant/config';


@Injectable()
export class SplashService {
    constructor(private http: Http) { }
    getActivityDetails(target: any) {
        return this.http.get(CONFIG.API_ENDPOINT + 'SelfSurveyServices/encryptedId/' + target)
            .map((res: Response) => {
                if (res) {
                    if (res.status === 204) {
                        return res;
                    } else if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch((error: HttpErrorResponse) => {
                if (error.status < 400 || error.status === 500) {
                    return Observable.throw(error);
                }
            });
    }
    getInstructionDetails(actTypeID: string) {
        return this.http.get(CONFIG.API_ENDPOINT_COMMONDATASERVICE + 'CommonDataService/rest/survey/instructions/' + actTypeID + "?isWebRequest=1")
            .map((res: Response) => {
                if (res) {
                    if (res.status === 204) {
                        return res;
                    } else if (res.status === 200) {
                        return res.json();
                    }
                }
            }).catch((error: HttpErrorResponse) => {
                if (error.status < 400 || error.status === 500) {
                    return Observable.throw(error);
                }
            });
    }
}