import {Component} from '@angular/core';
import {SessionService} from './constant/sessionService.component.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  showLoader:boolean;
  constructor(public _sessionService:SessionService){
    this._sessionService.isHeader = false;
    this._sessionService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }
}