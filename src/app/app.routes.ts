import { RouterModule, Routes } from '@angular/router';
import {
  AppComponent, UploadMedicalDocumentComponent, SuccessComponent, InstructionComponent, SplashComponent
} from './directives';

export const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { component: AppComponent, path: 'app' },
  { component: SplashComponent, path: 'splash' },
  { component: InstructionComponent, path: 'instruction' },
  { component: SuccessComponent, path: 'success' },
  { component: UploadMedicalDocumentComponent, path: 'uploadDocument' },
  { path: '**', redirectTo: 'splash', pathMatch: 'full' }
]