export {AppComponent} from './app.component'
export {SplashComponent} from './splash/splash.component'
export {InstructionComponent} from './instruction/instruction.component'
export {SuccessComponent} from './success/success.component'
export { UploadMedicalDocumentComponent } from './upload-medical-document/upload-medical-document.component';

